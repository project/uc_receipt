<html>
<head>
<style>
body {FONT-FAMILY: Arial;font-size:12px}
</style>
</head>
<body>
<table width="650" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width=170><p>&nbsp;ПОВІДОМЛЕННЯ</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p><br>
        &nbsp;Касир</p>
      </td>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width=75%>&nbsp;Отримувач платежу</td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td>&nbsp;<font color="#000000">[userm-name], [userm-address]</font></td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td align=right style="border-bottom: 1px;border-left: 0px;border-top: 0px;border-right: 0px; border-style: solid; border-color: Black;"><font color="#000000">[userm-rs]</font>&nbsp;</td>
          <td align=center style="border-bottom: 1px;border-right: 0px;border-top: 0px;border-left: 1px; border-style: solid; border-color: Black;"><font color="#000000">[userm-inn]</font></td>
        </tr>
        <tr> 
          <td align="right" style="font-size:10px">Поточний рахунок&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td align=center style="font-size:10px">Код ЄДРПОУ</td>
        </tr>
        <tr> 
          <td>&nbsp;Установа банку</td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td style="border-bottom: 1px;border-left: 0px;border-top: 0px;border-right: 0px; border-style: solid; border-color: Black;">&nbsp;<font color="#000000">[bank-name], [bank-address]</font></td>
          <td align=center style="border-bottom: 1px;border-left: 1px;border-top: 1px;border-right: 0px; border-style: solid; border-color: Black;"><font color="#000000">[bank-mfo]</font></td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
          <td align=center style="font-size:10px">МФО банку</td>
        </tr>
        <tr> 
          <td>&nbsp;Прізвище, ім'я та по-батькові, адреса платника:</td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td>&nbsp;<font color="#000000"><b>[order-last-name] [order-first-name], [address]</b></font></td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td style="border-bottom: 1px;border-left: 0px;border-top: 1px;border-right: 0px; border-style: solid; border-color: Black;">&nbsp;Вид платежу</td>
          <td  align=center style="border-bottom: 1px;border-left: 1px;border-top: 1px;border-right: 0px; border-style: solid; border-color: Black;">Сума</td>
        </tr>
        <tr> 
          <td style="border-bottom: 1px;border-left: 0px;border-top: 0px;border-right: 0px; border-style: solid; border-color: Black;">&nbsp;<font color="#000000"><b>[payment-pro]</b></font></td>
          <td  align=center style="border-bottom: 1px;border-left: 1px;border-top: 0px;border-right: 0px; border-style: solid; border-color: Black;">[receipt-total]грн.</td>
        </tr>
        <tr> 
          <td style="border-bottom: 1px;border-left: 0px;border-top: 0px;border-right: 0px; border-style: solid; border-color: Black;">&nbsp;Всього:</td>
          <td style="border-bottom: 1px;border-left: 1px;border-top: 0px;border-right: 0px; border-style: solid; border-color: Black;" align=center><font color="#000000"><b>[receipt-total]грн.</b></font></td>
        </tr>
        <tr>
          <td>&nbsp;Платник</td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td><p>&nbsp;ПОВІДОМЛЕННЯ</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p><br>
        &nbsp;Касир</p>
      </td>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width=75%>&nbsp;Отримувач платежу</td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td>&nbsp;&nbsp;<font color="#000000">[userm-name] [userm-address]</font></td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td align=right style="border-bottom: 1px;border-left: 0px;border-top: 0px;border-right: 0px; border-style: solid; border-color: Black;"><font color="#000000">[userm-rs]</font>&nbsp;</td>
          <td align=center style="border-bottom: 1px;border-right: 0px;border-top: 0px;border-left: 1px; border-style: solid; border-color: Black;"><font color="#000000">[userm-inn]</font></td>
        </tr>
        <tr> 
          <td align="right" style="font-size:10px">Поточний рахунок&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td  align=center style="font-size:10px">Код ЄДРПОУ</td>
        </tr>
        <tr> 
          <td>&nbsp;Установа банку</td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td style="border-bottom: 1px;border-left: 0px;border-top: 0px;border-right: 0px; border-style: solid; border-color: Black;">&nbsp;<font color="#000000">[bank-name], [bank-address]</font></td>
          <td  align=center style="border-bottom: 1px;border-left: 1px;border-top: 1px;border-right: 0px; border-style: solid; border-color: Black;"><font color="#000000">[bank-mfo]</font></td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
          <td  align=center style="font-size:10px">МФО банку</td>
        </tr>
        <tr> 
          <td> &nbsp;Прізвище, ім'я та по-батькові, адреса платника:</td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td>&nbsp;<font color="#000000"><b>[order-last-name] [order-first-name], [address]</b></font></td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td style="border-bottom: 1px;border-left: 0px;border-top: 1px;border-right: 0px; border-style: solid; border-color: Black;">&nbsp;Вид 
            платежу</td>
          <td  align=center style="border-bottom: 1px;border-left: 1px;border-top: 1px;border-right: 0px; border-style: solid; border-color: Black;">Сума</td>
        </tr>
        <tr> 
          <td style="border-bottom: 1px;border-left: 0px;border-top: 0px;border-right: 0px; border-style: solid; border-color: Black;">&nbsp;<font color="#000000"><b>[payment-pro]</b></font></td>
          <td  align=center style="border-bottom: 1px;border-left: 1px;border-top: 0px;border-right: 0px; border-style: solid; border-color: Black;">[receipt-total]грн.</td>
        </tr>
        <tr> 
          <td style="border-bottom: 1px;border-left: 0px;border-top: 0px;border-right: 0px; border-style: solid; border-color: Black;">&nbsp;Всього:</td>
          <td style="border-bottom: 1px;border-left: 1px;border-top: 0px;border-right: 0px; border-style: solid; border-color: Black;" align=center><font color="#000000"><b>[receipt-total]грн.</b></font></td>
        </tr>
        <tr>
          <td>&nbsp;Платник</td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
  </td>
  </tr>
</table>
</body>
</html>
